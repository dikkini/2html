from mongoengine import connect
from pyramid.config import Configurator
from views.views import GeneratePage, Index


def main(global_config, **settings):
    """
    This function returns a WSGI application.
    """
    config = Configurator(settings=settings)
    settings = dict(settings)
    settings.setdefault('jinja2.i18n.domain', '2html')
    connect(settings['db_name'])

    config.include('pyramid_jinja2')
    config.add_static_view('lib', 'templates/lib', cache_max_age=3600)

    config.add_route('index', '/index/')
    config.add_view(Index, route_name='index', renderer="index.jinja2")

    config.add_route('bannerPage', '/banner/{objectId}/{template}')
    config.add_view(GeneratePage, route_name='bannerPage')

    config.add_route('newbanner_route', '/getbanner')
    config.add_route('delbanner_route', '/delbanner')
    config.add_route('editbanner_route', '/editbanner')


    config.scan()
    return config.make_wsgi_app()
