# coding=utf-8
"""
Файл представлений, реденринг всех страниц
"""

import logging
from mongoengine import ValidationError
from pyramid.httpexceptions import HTTPNotFound, HTTPFound
from pyramid.i18n import TranslationStringFactory
from pyramid.renderers import render_to_response
from pyramid.view import view_config
from ..ORM.mongo import GetBanners, SaveBanner, GetBanner, DelBanner, EditBanner
from ..ORM.models import Banner
from ..services.addbanner import getTemplates
from ..services.html import GetImageFromBanner, ConstructNewBanner

try:
    logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s', level=logging.DEBUG,
                        filename=u'2html/log')
except IOError, e:
    print e
    print "Нет файла логов!"

_ = TranslationStringFactory('2html')


def Index(request):
    """
    """
    templates = getTemplates()
    banners = GetBanners()
    return {"banners": banners, "templates": templates}


def GeneratePage(request):
    bannerId = request.matchdict['objectId']
    template = request.matchdict['template']
    newBanner = GetBanner(bannerId)
    return render_to_response(template, {"newBanner": newBanner}, request=request)


@view_config(route_name='newbanner_route', request_method='POST')
def GenerateNewBanner(request):
    oldbanner = request.params['oldbanner'].decode('utf-8')
    newbannerhref = request.params['newbannerhref'].decode('utf-8')
    template = request.params['template'].decode('utf-8')
    newbannerimg = GetImageFromBanner(oldbanner)
    newbanner = ConstructNewBanner(newbannerimg, newbannerhref)
    SaveBanner(oldbanner, newbanner, newbannerhref, template)
    return HTTPFound(location='/index/')


@view_config(route_name='delbanner_route', request_method='POST')
def DBanner(request):
    bannerId = request.params['bannerId'].decode('utf-8')
    DelBanner(bannerId)
    return HTTPFound(location='/index/')


@view_config(route_name='editbanner_route', request_method='POST')
def EBanner(request):
    bannerId = request.params['bannerId'].decode('utf-8')
    oldbanner = request.params['oldbanner'].decode('utf-8')
    newbannerhref = request.params['newbannerhref'].decode('utf-8')
    template = request.params['template'].decode('utf-8')
    newbannerimg = GetImageFromBanner(oldbanner)
    newbanner = ConstructNewBanner(newbannerimg, newbannerhref)
    EditBanner(bannerId, oldbanner, newbanner, newbannerhref, template)
    return HTTPFound(location='/index/')