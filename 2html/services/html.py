# coding=utf-8
"""
Файл бизнес логики приложения
"""

import hashlib
import random
import socket
import time
import lxml.html

__author__ = 'haribo'


def GetImageFromBanner(banner):
    """

    """
    img_src = None
    try:
        doc = lxml.html.document_fromstring(banner)

        for img in doc.cssselect('a img'):
            img_src = img.get('src')
    except Exception, e:
        print u"Ошибка при парсинге HTML. Проверьте наличие тегов <a> и <img>!", e

    return img_src


def GenerateUUID(*args):
    """
    Generates a universally unique ID.
    Any arguments only create more randomness.
    """
    t = long(time.time() * 1000)
    r = long(random.random() * 100000000000000000L)
    try:
        a = socket.gethostbyname(socket.gethostname())
    except Exception:
        # if we can't get a network address, just imagine one
        a = random.random() * 100000000000000000L
    uuid = str(t) + ' ' + str(r) + ' ' + str(a) + ' ' + str(args)
    uuid = hashlib.md5(uuid).hexdigest()

    return uuid


def ConstructNewBanner(img, href):
    return '<p><a href="' + href + '"><img src="' + img + '" width="100" height="100"></a></p>'