import random
import sys
import os

__author__ = 'haribo'


def getTemplates():
    directory = 'C:\\devspace\\python\\kalinin-project\\2html\\2html\\templates'
    files = os.listdir(directory)
    templates = filter(lambda x: x.endswith('.jinja2') and x != 'base.jinja2' and x != 'index.jinja2', files)
    return templates