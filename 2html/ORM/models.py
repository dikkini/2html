# coding=utf-8
"""
Файл моделей объектов выгружаемых в базу данных
"""

from mongoengine import Document, StringField


def _assign(obj, name, parent):
    obj.__name__ = name
    obj.__parent__ = parent
    return obj


class Banner(Document):
    oldBanner = StringField(required=True)
    newBannerHref = StringField(required=True)
    newBanner = StringField()
    link = StringField()
    templateName = StringField(required=True)


class BannerId(Banner):
    link = StringField(required=True)

