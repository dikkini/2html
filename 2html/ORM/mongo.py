# coding=utf-8
"""
Файл работы с базой данных
"""

from models import Banner, BannerId

__author__ = 'haribo'


def SaveBanner(oldBanner, newBanner, newBannerHref, templateName):
    """

    """
    data = Banner()
    data.newBanner = newBanner
    data.newBannerHref = newBannerHref
    data.oldBanner = oldBanner
    data.templateName = templateName
    data.save()
    return data.id


def GetBanners():
    """

    """
    banners = []
    for data in Banner.objects:
        banners.append(data)

    return banners


def GetBanner(objectId):
    for banner in Banner.objects(id=objectId):
        return banner.newBanner


def DelBanner(objectId):
    banner = Banner.objects(id=objectId)
    banner.delete()


def EditBanner(objectId, oldBanner, newBanner, newBannerHref, templateName):
    data = Banner(id=objectId)
    data.newBanner = newBanner
    data.newBannerHref = newBannerHref
    data.oldBanner = oldBanner
    data.templateName = templateName
    data.save()